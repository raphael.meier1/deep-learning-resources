# Resources for the Deep Learning Competence in the Data Science Bachelor Program at FHNW  

This is the repository for Deep Learning resources in the data science program at FHNW.

Nice introduction to the field: [3Blue1Brown - SEASON 3](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)

## Online Courses / Tutorials

### Coursera "Deep Learning Specialization"
by Andrew Ng <br>
see [course](https://www.coursera.org/specializations/deep-learning?utm_source=gg&utm_medium=sem&utm_campaign=07-StanfordML-ROW&utm_content=07-StanfordML-ROW&campaignid=2070742271&adgroupid=73238990061&device=c&keyword=machine%20learning%20tutorial%20for%20beginners&matchtype=b&network=g&devicemodel=&adpostion=&creativeid=369041663030&hide_mobile_promo=&gclid=Cj0KCQiA962BBhCzARIsAIpWEL1buAhH9NKSaVcrv_V36bzsE_ESFe5j7sKdkG5Q9rZOqRTZYDHrMP4aAnLLEALw_wcB#courses)
- Courses 1-3 (all weeks), Course 4 (weeks 1+2), Course 5 (week1)
- Framework: None
- Coursera "DeepLearning.AI TensorFlow" (with a strong connection to Andrew Ng's course): see [course](https://www.coursera.org/professional-certificates/tensorflow-in-practice) 
- Starts quite with the basics and advances quite far.


### Stanford University "Convolutional Neural Networks for Visual Recognition"
by Fei Fei Lee, Justin Johnson, Serena Yeung - originally prepared by Andrey Karpathy)<br>
see [course](http://cs231n.stanford.edu/2019/)
- Lectures 1-10
- Framework: None
- Possibly a bit more advanced - gives several very nice insights and examples 


### Francois Fleuret Lectures at University of Geneva
see [course](https://fleuret.org/dlc/)
- Lectures 1-6, 7.1, 7.2, 8.1, 8.2, 9, 12
- Framework: PyTorch
- Very clear and concise, sometimes a bit more advanced
- See also his "liitle book of deeplearning": https://fleuret.org/public/lbdl.pdf  


### UW-Madison "Deep Learning and Generative Models"
by Sebastian Raschka<br>
see https://sebastianraschka.com/
- [syllabus](http://pages.stat.wisc.edu/~sraschka/teaching/stat453-ss2021/)
- [youtube playlist](https://www.youtube.com/playlist?list=PLTKMiZHVd_2KJtIXOW0zFhFfBaJJilH51)


### University of Amsterdam "Deep Learning Tutorials"
by Philipp Lippe<br>
see https://uvadlc-notebooks.readthedocs.io/en/latest/index.html


### CDS@NYU "Deep Learning"
by Y LeCun and A Canziani<br>
see [course](https://atcold.github.io/pytorch-Deep-Learning/)
- Weeks 1-7, week 14.3, possibly week 11 (1+2)
- Framework: PyTorch


## Other Online Resources
- Aurélien Géron, "Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow": see [Jupyter Notebooks](https://github.com/ageron/handson-ml2) with Tensorflow 2
- Eli Stevens, et al "Deep Learning with PyTorch": see [pdf](https://pytorch.org/assets/deep-learning/Deep-Learning-with-PyTorch.pdf)
- Ian Goodfellow et al "Deep Learning": see [pdf](https://www.deeplearningbook.org/)
- Michael Nielsen: http://neuralnetworksanddeeplearning.com
- Chris Olah: https://colah.github.io/ -> very nice illustrations!
- Simon J.D. Prince: https://udlbook.github.io/udlbook/ -> very new book!


### MSE Course Deep Learning
- by Martin Melchior and Jean Hennebert
- [Slides](https://gitlab.fhnw.ch/deep_learning/sgds/resources/-/tree/master/mse_lecture_slides_hs19) (organized by LEs, based on numpy and keras)
- [Slides](https://gitlab.fhnw.ch/deep_learning/sgds/resources/-/tree/master/mse_lecture_slides_fs22) (organized by LEs, based on pytorch and keras)
- [Recordings](https://gitlab.fhnw.ch/deep_learning/sgds/resources/-/blob/master/mse_lecture_slides/MSE_DL_Recordings_SS21.pdf) from SS21.


## Other
- [Cheat Sheets](https://becominghuman.ai/cheat-sheets-for-ai-neural-networks-machine-learning-deep-learning-big-data-678c51b4b463)
- [Various Resources](https://towardsdatascience.com/supercharge-data-science-562d891ef7f9)
- [Paper roadmap](https://github.com/floodsung/Deep-Learning-Papers-Reading-Roadmap)



